//
//  MapQuadTree.swift
//  LookAroundYou
//
//  Created by logen on 2/17/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

import Foundation

class MapQuadTree : QuadTree {
    let mapView: GMSMapView
    
    init(map: GMSMapView, box: BoundingBox) {
        mapView = map
        super.init(box: box)
    }
   
   func clustersForBox(box: BoundingBox) -> [String : [Place]] {
      let scaler = mapScaler(box, zoom:self.mapView.camera.zoom)
      let xMin = floor(box.xLeft * scaler)
      let xMax = ceil(box.xRight * scaler)
      let yMin = floor(box.yBottom * scaler)
      let yMax = ceil(box.yTop * scaler)

      var clusters = [String:[Place]]()
      for var xIndex = xMin; xIndex <= xMax; xIndex++ {
         for var yIndex = yMin; yIndex <= yMax; yIndex++ {
            let xLeft = xIndex / scaler
            let xRight = xLeft + 1.0 / scaler
            let yBottom = yIndex / scaler
            let yTop = yBottom + 1.0 / scaler
            
            let cell = BoundingBox(xLeft: xLeft, xRight: xRight, yBottom: yBottom, yTop: yTop)
            let places = self.queryRange(cell)
            
            // TODO (watk) -- nothing is currently removed until zoomed
            if places.count > 0 {
               // Update & reposition existing marker, or create one if nonexistant
               let topLeftId = CLLocationCoordinate2D(latitude: yTop, longitude: xLeft).stringId
               clusters[topLeftId] = places
            }
         }
      }
      return clusters
   }
   
   // Used to tie a relationship between screen size/visible region/grid size
   func mapScaler(box: BoundingBox, zoom: Float) -> Double {
      var customZoom: Double
      switch(zoom) {
      case 9...13:
         customZoom = 96
      case 12...16:
         customZoom = 64
      case 16...25:
         customZoom = 32
      default:
         customZoom = 128
      }

      let screenWidth = Double(mapView.bounds.maxX)
      let coordinateWidth = box.xRight - box.xLeft
      return screenWidth / coordinateWidth / customZoom
   }
}