//
//  QuadTree.swift
//  LookAroundYou
//
//  Created by logen on 2/11/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

import Foundation

// TODO (watk)
// For reference: http://www.colorado.edu/geography/gcraft/notes/mapproj/gif/unproj.gif

struct QuadTreeNodeData {
    var location: CLLocationCoordinate2D
    var data: Place
}

struct BoundingBox {
    var xLeft: Double
    var xRight: Double
    var yBottom: Double
    var yTop: Double

    func containsLocation(location: CLLocationCoordinate2D) -> Bool {
        return
            xLeft < location.longitude
            && xRight > location.longitude
            && yBottom < location.latitude
            && yTop > location.latitude
    }
    
    func intersectsBox(box: BoundingBox) -> Bool {
        return
            xLeft < box.xRight
            && box.xLeft < xRight
            && yBottom < box.yTop
            && box.yBottom < yTop
    }
}

class QuadTree {
    let bounds: BoundingBox
    let nodeCapacity: Int = 4
    var nodes = [QuadTreeNodeData]()
    
    var northWest: QuadTree?
    var northEast: QuadTree?
    var southEast: QuadTree?
    var southWest: QuadTree?
    
    init(box: BoundingBox) {
        bounds = BoundingBox(xLeft: box.xLeft, xRight: box.xRight, yBottom: box.yBottom, yTop: box.yTop)
    }

    func insertNode(newNode: QuadTreeNodeData) -> Bool {
        if !bounds.containsLocation(newNode.location) {
            return false
        }

        // Don't insert if it already exists
        for node in nodes {
            if node.data.placeId == newNode.data.placeId {
                return false
            }
        }

        // Add to current quad is under capacity
        if (nodes.count < nodeCapacity) {
            nodes.append(newNode)
            return true
        }
        
        // Node needs to be added to a sub-quad
        if northWest == nil {
            subdivide()
        }
        
        // Try adding the node to each quad successively
        if (northWest?.insertNode(newNode) == true) { return true }
        if (northEast?.insertNode(newNode) == true) { return true }
        if (southEast?.insertNode(newNode) == true) { return true }
        if (southWest?.insertNode(newNode) == true) { return true }

        return false
    }

    func queryRange(box: BoundingBox) -> [Place] {
        // Stop if search ranges doesn't intersect this node
        if !bounds.intersectsBox(box) {
            return []
        }

        // Add matching nodes
        var places = [Place]()
        for node in nodes {
            if box.containsLocation(node.location) {
                places.append(node.data)
            }
        }
        
        // Add matching nodes from sub-quads
        if northWest != nil {
            places += northWest?.queryRange(box) ?? []
            places += northEast?.queryRange(box) ?? []
            places += southEast?.queryRange(box) ?? []
            places += southWest?.queryRange(box) ?? []
        }
        return places
    }

    func subdivide() {
        let xMid = (bounds.xLeft + bounds.xRight) / 2.0
        let yMid = (bounds.yBottom + bounds.yTop) / 2.0
        
        // Break this quad into 4 equally divided quads
        let northWest = BoundingBox(xLeft: bounds.xLeft, xRight: xMid, yBottom: yMid, yTop: bounds.yTop)
        self.northWest = QuadTree(box: northWest)

        let northEast = BoundingBox(xLeft: xMid, xRight: bounds.xRight, yBottom: yMid, yTop: bounds.yTop)
        self.northEast = QuadTree(box: northEast)
        
        let southEast = BoundingBox(xLeft: xMid, xRight: bounds.xRight, yBottom: bounds.yBottom, yTop: yMid)
        self.southEast = QuadTree(box: southEast)
        
        let southWest = BoundingBox(xLeft: bounds.xLeft, xRight: xMid, yBottom: bounds.yBottom, yTop: yMid)
        self.southWest = QuadTree(box: southWest)
    }
}