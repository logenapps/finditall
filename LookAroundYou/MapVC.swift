//
//  MapVC.swift
//  LookAroundYou
//
//  Created by logen on 1/28/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreData

class MapVC : UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate, TypesVCDelegate, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var mapPinVerticalConstraint: NSLayoutConstraint!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var navTitleButton: UIButton!
    
    var allTypes = [String]()
    let clustersQueue: dispatch_queue_t
    var currentLocationButton: UIButton!
    var googleLogo: UIButton!
    var lastZoomLevel: Float = 0
    let locationManager = CLLocationManager()
    var mapsData: MapsData?
    var markers = [String: PlacesMarker]()
    let placeTable = PlaceTable()
    var rootQuad: MapQuadTree!
    var selectedPlaces = [Place]()
    var selectedTypes = [String]()
    
    var mapRadius: Double {
        get {
            let visibleRegion = mapView.projection.visibleRegion()
            let verticalDistance = GMSGeometryDistance(visibleRegion.nearLeft, visibleRegion.farLeft)
            let horizontalDistance = GMSGeometryDistance(visibleRegion.farRight, visibleRegion.farLeft)
            return max(horizontalDistance, verticalDistance) * 0.5
        }
    }

    var visibleBox: BoundingBox {
        get {
            let visibleRegion = mapView.projection.visibleRegion()
            return BoundingBox(
                xLeft: visibleRegion.farLeft.longitude,
                xRight: visibleRegion.farRight.longitude,
                yBottom: visibleRegion.nearLeft.latitude,
                yTop: visibleRegion.farLeft.latitude)
        }
    }
    
    @IBAction func actionPressed(sender: AnyObject) {
        //         TODO (watk) - clear markers while testing
        mapView.clear()
        markers = [:]
    }
    
    required init(coder aDecoder: NSCoder) {
        clustersQueue = dispatch_queue_create("com.logenapps.clusters", DISPATCH_QUEUE_SERIAL);
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        mapView.delegate = self
        mapView.settings.rotateGestures = false
        lastZoomLevel = self.mapView.camera.zoom

        // Initital setup for QuadTree -- the whole world as the box
        let worldBounds = BoundingBox(xLeft: -180, xRight: 180, yBottom: -90, yTop: 90)
        rootQuad = MapQuadTree(map: mapView, box: worldBounds)
        
        // CoreData fetch could always be used. But to show protocol use, I'll split this functionality
        println("Updating selected types from persistent store")
        let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
        let managedContext = appDelegate.managedObjectContext!
        let fetchRequest = NSFetchRequest(entityName: "Type")
        var error: NSError?
        let fetchedResults = managedContext.executeFetchRequest(fetchRequest, error: &error) as [Type]?
        if (fetchedResults != nil) {
            for type in fetchedResults! {
                allTypes.append(type.apiName)
                if (true == type.selected) {
                    selectedTypes.append(type.apiName)
                }
            }
            mapsData = MapsData(selectedTypes: selectedTypes)
        } else {
            println("Error fetching Types: \(error)")
        }
        
        // Google insists that their logo not be obscured, but only provides map padding
        //  to make that work with an app's UI. That is a poor UX with dynamic UI elements.
        // This app grabs a specific reference to each element.
        currentLocationButton = mapView.subviews[1].subviews[2] as UIButton
        googleLogo = mapView.subviews[1].subviews[3] as UIButton
        mapView.settings.compassButton = true
        
        // This table will start below the screen, but animate in when places are tapped
        updateTablePosition()
        let nibName = UINib(nibName: "PlaceCell", bundle:nil)
        placeTable.registerNib(nibName, forCellReuseIdentifier: "PlaceCell")
        placeTable.delegate = self
        placeTable.dataSource = self
        self.view.addSubview(placeTable)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showTypes" {
            let navigationController = segue.destinationViewController as UINavigationController
            let controller = segue.destinationViewController.topViewController as TypesVC
            controller.delegate = self
        }
    }
    
    func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .AuthorizedWhenInUse  {
            locationManager.startUpdatingLocation()
            mapView.myLocationEnabled = true
            mapView.settings.myLocationButton = true
        }
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        if let location = locations.last as? CLLocation {
            // Default to user's location, but then don't repeatedly update location
            mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            locationManager.stopUpdatingLocation()
        }
    }
    
    func mapView(mapView: GMSMapView!, idleAtCameraPosition position: GMSCameraPosition!) {
        // If zoom level changes, clear markers
        if lastZoomLevel != self.mapView.camera.zoom {1
            markers = [:]
            self.mapView.clear()
            lastZoomLevel = self.mapView.camera.zoom
        }

        // The navigation title is updated with simple location  information
        GMSGeocoder().reverseGeocodeCoordinate(position.target) { response, error in
            var titleParts = [String]()
            if let address = response?.firstResult() {
                if let locality = address.locality {
                    titleParts.append(locality)
                }
                if let area = address.administrativeArea {
                    titleParts.append(area)
                }
            }
            let title = join(", ", titleParts)
            self.navigationItem.title = title
        }

        // Kick off cluster update, and start fetch for new places
        dispatch_async(self.clustersQueue) {
            self.updateClusters()
        }
        fetchPlacesForCoordinate(position.target, radius: mapRadius, retryCount: 0)
    }

    func updateCameraForMarker(marker: PlacesMarker) {
        // Zoom-level varies based on both
        lastZoomLevel = mapView.camera.zoom
        var newZoom: Float
        switch(lastZoomLevel, marker.places.count) {
        case (0...10, _):
            // If zoomed this far out, zoom in regardless of count
            newZoom = lastZoomLevel + 2.5
        case (18...25, _):
            // If zoomed this far in, don't zoom in regardless of count
            newZoom = lastZoomLevel
        case (_, 4...1000):
            // Otherwise, if count > 6, also zoom in
            newZoom = lastZoomLevel + 1.5
        default:
            newZoom = lastZoomLevel
        }

        // Adjust the camera position
        let cameraPosition = GMSCameraPosition(
            target: CLLocationCoordinate2DMake(marker.position.latitude, marker.position.longitude),
            zoom: newZoom,
            bearing: mapView.camera.bearing,
            viewingAngle: mapView.camera.viewingAngle)
        mapView.animateToCameraPosition(cameraPosition)
    }

    func mapView(mapView: GMSMapView!, didTapMarker marker: PlacesMarker!) -> Bool {
        updateCameraForMarker(marker)

        selectedPlaces = marker.places
        placeTable.reloadData()
        updateTablePosition()
        return true
    }

    func mapView(mapView: GMSMapView!, didTapAtCoordinate coordinate: CLLocationCoordinate2D) {
        selectedPlaces = []
        updateTablePosition()
    }

    func fetchPlacesForCoordinate(coordinate: CLLocationCoordinate2D, radius: Double, retryCount: Int, token: String?=nil) {
        mapsData?.nearbySearchForCoordinate(coordinate, radius: radius, retryCount: retryCount, token:token) { places, retryCount, token in
            var newRetry = retryCount
            // Google requires a brief delay before retrieving additional results, but that time is dynamic roughly 1-2 seconds
            // If a token was returned but no places were, the last call was made too quickly. Keep trying to a limit
            //   If places were returned, the last call was successful and the retry count is reset
            // If the camera position has changed, stop fetching results for that target
            if let nextToken = token {
                let retryLimit = 10
                if places.count == 0 {
                    newRetry++
                } else {
                    newRetry = 0
                }
                
                if newRetry < retryLimit && coordinate.isEqual(self.mapView.camera.target) {
                    let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.21 * Double(NSEC_PER_SEC)))
                    dispatch_after(delayTime, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                        self.fetchPlacesForCoordinate(coordinate, radius: radius, retryCount: newRetry, token: nextToken)
                    }
                }
            }

            if places.count > 0 {
                // Ensure QuadTree is up to date with all places (dupes are ignored internally)
                dispatch_async(self.clustersQueue) {
                    for (placeId, place) in places {
                        self.rootQuad.insertNode(QuadTreeNodeData(location: place.location, data: place))
                    }
                    self.updateClusters()
                }
            }
        }
    }

    // visibleBox requires main thread,
    //  then process clusters in background,
    //  then update the map on main thread
    func updateClusters() {
        dispatch_async(dispatch_get_main_queue()) {
            let searchBox = self.visibleBox
            dispatch_async(self.clustersQueue) {
                let clusters = self.rootQuad.clustersForBox(searchBox)
                dispatch_async(dispatch_get_main_queue()) {
                    for (markerId, places) in clusters {
                        if let existingMarker = self.markers[markerId] {
                            existingMarker.updateMarker(places)
                        } else {
                            let newMarker = PlacesMarker(places: places)
                            self.markers[markerId] = newMarker
                            newMarker.map = self.mapView
                        }
                    }
                }
            }
        }
    }

    // Protocol methods
    // TypesVC will inform delegates of new selected types through delegate
    func updateSelectedTypes(types: [String]) {
        selectedTypes = types
        fetchPlacesForCoordinate(mapView.camera.target, radius: mapRadius, retryCount: 0)
    }

    // Table methods
    func updateTablePosition() {
        let rowHeight: CGFloat = 60.0
        let maxVisibleRows = Int(self.view.frame.height * 0.33 / rowHeight)
        let maxTableHeight: CGFloat = CGFloat(maxVisibleRows) * rowHeight
        var tableHeight = CGFloat(min(selectedPlaces.count, maxVisibleRows)) * rowHeight
        if selectedPlaces.count > maxVisibleRows {
            // Show that additional items are in table view
            tableHeight += 0.25 * rowHeight
        } else if selectedPlaces.count != 0 {
            // Don't show bottom separator if other items exist
            tableHeight -= 1
        }
        
        // Need to move not only the table view, but also the Google map elements
        let previousTableHeight = self.placeTable.frame.height
        let heightDifference = previousTableHeight - tableHeight
        var logoFrame = googleLogo.frame
        var buttonFrame = currentLocationButton.frame
        logoFrame.origin.y += heightDifference
        buttonFrame.origin.y += heightDifference
        
        UIView.animateWithDuration(0.15,
            delay: 0.0,
            options: .CurveEaseOut,
            animations: {
                self.placeTable.frame = CGRectMake(0, self.view.frame.height - tableHeight , self.view.frame.width, tableHeight)
                self.googleLogo.frame = logoFrame
                self.currentLocationButton.frame = buttonFrame
            },
            completion: { finished in
        })
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedPlaces.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("PlaceCell", forIndexPath: indexPath) as PlaceCell
        let place = selectedPlaces[indexPath.row]
        cell.nameLabel.text = place.name
        cell.descriptionLabel.text = place.vicinity
        cell.typeImage.image = place.typeImage
        return cell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        // Extends separator for entire cell width
        cell.separatorInset = UIEdgeInsetsZero
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = false;
    }
    
}

