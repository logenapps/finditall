//
//  Type.swift
//  LookAroundYou
//
//  Created by logen on 2/2/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

import Foundation
import CoreData

@objc(Type)
class Type: NSManagedObject {

    @NSManaged var apiName: String
    @NSManaged var displayName: String
    @NSManaged var selected: Bool

}
