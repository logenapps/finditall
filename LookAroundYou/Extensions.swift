//
//  Extensions.swift
//  LookAroundYou
//
//  Created by logen on 2/18/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

import Foundation

extension CLLocationCoordinate2D {
    var stringId: String {
        return String(format: "%.7f-%.7f", self.latitude, self.longitude)
    }
    
    func isEqual(other: CLLocationCoordinate2D) -> Bool {
        return self.latitude == other.latitude && self.longitude == self.longitude
    }
}