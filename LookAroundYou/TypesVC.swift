//
//  CategoriesVC.swift
//  LookAroundYou
//
//  Created by logen on 1/28/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

import UIKit
import CoreData

protocol TypesVCDelegate: class {
    func updateSelectedTypes(types: [String])
}

class TypesVC: UITableViewController {
    weak var delegate: TypesVCDelegate?
    var allTypes = [String]()
    var sortedTypes = [Type]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Fetch the type list
        let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
        let managedContext = appDelegate.managedObjectContext!
        let fetchRequest = NSFetchRequest(entityName: "Type")
        let sortDescriptor = NSSortDescriptor(key: "displayName", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        var error: NSError?
        let fetchedResults = managedContext.executeFetchRequest(fetchRequest, error: &error) as [Type]?
        if (fetchedResults != nil) {
            sortedTypes = fetchedResults!
            for type in sortedTypes {
                allTypes.append(type.displayName)
            }
        } else {
            println("Error fetching Types: \(error)")
        }
    }

    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortedTypes.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("TypesCell", forIndexPath: indexPath) as UITableViewCell
        let type = sortedTypes[indexPath.row]
        cell.textLabel?.text = type.displayName
        //todo(watk)
        // how am i showing selected/image?
        cell.accessoryType = type.selected ? .Checkmark : .None
        
        // TODO (watk) -- i don't have real images for each type, so using this hacky method
        let firstLetter = type.apiName[type.apiName.startIndex]
        if let newImage = UIImage(named: "\(firstLetter)_square_50") {
            cell.imageView?.image = newImage
        } else {
            cell.imageView?.image = UIImage(named: "a_square_50")
        }
        cell.imageView?.contentMode = .ScaleAspectFit

        return cell
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let type = sortedTypes[indexPath.row]
        type.selected = !type.selected
        
        let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
        let managedContext = appDelegate.managedObjectContext!
        var error: NSError?
        managedContext.save(&error)
        
        tableView.reloadData()
    }
    
    override func viewWillDisappear(animated: Bool) {
        var selectedTypes = [String]()
        for type in sortedTypes {
            if (type.selected == true) {
                selectedTypes.append(type.apiName)
            }
        }
        delegate?.updateSelectedTypes(selectedTypes)
    }
}