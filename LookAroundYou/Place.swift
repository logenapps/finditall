//
//  Place.swift
//  LookAroundYou
//
//  Created by logen on 1/31/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

import Foundation
import SwiftyJSON

class Place {
    let name: String
    let placeId: String
    let placeTypes = [String]()
    let location: CLLocationCoordinate2D
    var primaryType = String()
    var typeImage = UIImage()
    let vicinity: String

    init(data: JSON, selectedTypes: [String]) {
        name = data["name"].stringValue
        placeId = data["place_id"].stringValue
        vicinity = data["vicinity"].stringValue
        
        let lat = data["geometry"]["location"]["lat"].doubleValue
        let lng = data["geometry"]["location"]["lng"].doubleValue
        location = CLLocationCoordinate2D(latitude: lat, longitude: lng)

        for (index: String, rawType: JSON) in data["types"] {
            placeTypes.append(rawType.string!)
        }
        updateForSelectedTypes(selectedTypes)
    }
    
    func updatePrimaryType(selectedTypes: [String]) {
        for type in placeTypes {
            if contains(selectedTypes, type) {
                primaryType = type
                break
            }
        }
    }
    
    func updateTypeImage() {
        var firstLetter = primaryType[primaryType.startIndex]
        if let newImage = UIImage(named: "\(firstLetter)_square_50") {
            typeImage = newImage
        } else {
            typeImage = UIImage(named: "a_square_50")!
        }
    }
    
    // If type selections change, primary type and image may need to change as well
    func updateForSelectedTypes(selectedTypes: [String]) {
        updatePrimaryType(selectedTypes)
        updateTypeImage()
    }
}
