//
//  MapsData.swift
//  LookAroundYou
//
//  Created by logen on 1/28/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

import Foundation
import SwiftyJSON

class MapsData {
    let apiKey = "AIzaSyAWw_0maAhcLDw_n0jzKvc0VwvxWmZvXJE"
    var placesTask = NSURLSessionDataTask()
    let selectedTypes: [String]

    init(selectedTypes: [String]) {
        self.selectedTypes = selectedTypes
    }
    
    // For testing, returning dictionary of places
    func nearbySearchForCoordinate(coordinate: CLLocationCoordinate2D, radius: Double, retryCount: Int, token: String?, completionHandler: ([String: Place], Int, String?) -> ()) {
        let typesString = selectedTypes.count > 0 ? join("|", selectedTypes) : "restaurant"
        var apiURL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=\(apiKey)&location=\(coordinate.latitude),\(coordinate.longitude)&radius=\(radius)&rankby=prominence&types=\(typesString)"
        if let nextToken = token {
            apiURL += "&pagetoken=\(nextToken)"
        }
        apiURL = apiURL.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!

        // New searches cancel active running, but repeat searches cannot
        // Long-term this should be more robust but it works reasonably well
        if placesTask.taskIdentifier > 0 && placesTask.state == .Running && token == nil {
            placesTask.cancel()
        }

        placesTask = NSURLSession.sharedSession().dataTaskWithURL(NSURL(string: apiURL)!) {data, response, error in
            if let errorFound = error {
                // Cancelled is valid, used to reduce network requests
                if errorFound.code != NSURLErrorCancelled {
                    println("Error in \(__FUNCTION__). Error: \(errorFound)")
                    println("result: \(response)")
                }
            } else {
                if let rawJson: AnyObject = NSJSONSerialization.JSONObjectWithData(data, options:nil, error:nil) {
                    var json = JSON(rawJson)
                    let status = json["status"].stringValue
                    var places = [String: Place]()
                    if status == "OK" {
                        for (index: String, rawPlace: JSON) in json["results"] {
                            let place = Place(data: rawPlace, selectedTypes:self.selectedTypes)
                            places[place.placeId] = place
                        }
                        completionHandler(places, retryCount, json["next_page_token"].string)
                    } else {
                        // If the request failed, it is likely because the request didn't have adequate delay.
                        // the completionHandler will decide whether or not to try again using the same token
                        completionHandler(places, retryCount, token)
                    }
                }
            }
        }
        placesTask.resume()
    }
}