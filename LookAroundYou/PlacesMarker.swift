//
//  PlaceMarker.swift
//  LookAroundYou
//
//  Created by logen on 2/8/15.
//  Copyright (c) 2015 logenw. All rights reserved.
//

import Foundation


class PlacesMarker: GMSMarker {
    var places: [Place]

    init(places: [Place]) {
        self.places = places
        super.init()

        appearAnimation = kGMSMarkerAnimationPop
        updateIcon()
        updatePosition()
    }
    
    func updateMarker(newPlaces: [Place]) {
        places = sorted(newPlaces, { p1, p2 in return p1.name < p2.name } )
        updateIcon()
        updatePosition()
    }
    
    func updatePosition() {
        let count = Double(places.count)
        var latitudeSum: Double = 0
        var longitudeSum: Double = 0
        for place in places {
            latitudeSum += place.location.latitude
            longitudeSum += place.location.longitude
        }
        position = CLLocationCoordinate2D(latitude: latitudeSum / count, longitude: longitudeSum / count)
    }
    
    func updateIcon() {
        // TODO (watk) Use a specific image for a single hit, cluster otherwise
        let count = Double(places.count)
        if count == 1 {
            // TODO (watk) -- I don't have real images for each type, this hacky method is used instead
            var firstLetter = places[0].primaryType[places[0].primaryType.startIndex]
            if let newImage = UIImage(named: "\(firstLetter)_circle_25") {
                icon = newImage
            } else {
                icon = UIImage(named: "a_circle_25")
            }
        } else {
            //todo watk using "b" for all these currently
            icon = UIImage(named: "b_circle_25")
        }
    }
}