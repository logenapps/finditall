# Find It All/Look Around You #

This app provides a map view that shows nearby businesses (restaurants, gas stations, etc...) based on the current map location and zoom level.

### Features ###

* See nearby businesses of interest relative to your current location
* Customize the types of businesses you want to see
* If too many businesses are within a small area to display cleanly, bundle those elements into a single, tappable marker.

### Tech Used ###

* Swift programming language (1.x)
* Retrieving, parsing, and utilizing data from Google Places API with async tasks.
* "QuadTree" data structure built to maintain the information on those places in an efficient manner.
* Display relevant locations on the user's map using the Google Maps API
* CoreData used for storing user preferences (intention was just to see how simply a pre-loaded database would work), as well as sending data through protocols.

### Note on project status ###

* This project is not being maintained.